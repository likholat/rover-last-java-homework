public class Rectangle implements Shape {
    private double length = 10;
    private double width = 5;

 public double getArea(){
     return length*width;
 }

 public String toString(){
     String str = "length = " + length + " " + "width = " + width + " " + "Area is = " + getArea();
     return str;
 }

    @Override
    public int hashCode() {
        int tmp = 31;
        int res = 1;
        res = (int)(res*tmp+length);
        res = (int)(res*tmp+width);
        return res;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass()!= obj.getClass())
            return false;
        Rectangle tr = (Rectangle) obj;
        if (width != tr.width)
            return false;
        if (length != tr.length)
            return false;
        return true;
    }
}
