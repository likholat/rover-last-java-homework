public class Triangle implements Shape {
    private double base = 6;
    private double height = 7;

    public double getArea(){
        return 0.5*base*height;
    }

    public String toString(){
        String str = "base = " + base + " " + "height = " + height + " " + "Area is = " + getArea();
        return str;
    }

    @Override
    public int hashCode() {
        int tmp = 31;
        int res = 1;
        res = (int)(res*tmp+base);
        res = (int)(res*tmp+height);
        return res;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass()!= obj.getClass())
            return false;
        Triangle tr = (Triangle) obj;
        if (base != tr.base)
            return false;
        if (height != tr.height)
            return false;
        return true;
    }
}
